/* eslint-disable no-magic-numbers */
import { ExchangeService } from '../../../services/ExchangeService'
const service = new ExchangeService()

describe('Test happy case Exchange logic', () => {
  const testCases = [
    {
      payAmount: 35,
      getMoney: 100,
      expect: {
        'bankNotes50': 1,
        'coins10': 1,
        'coins5': 1,
      },
    },
    {
      payAmount: 319,
      getMoney: 500,
      expect: {
        'bankNotes100': 1,
        'bankNotes50': 1,
        'bankNotes20': 1,
        'coins10': 1,
        'coins1': 1,
      },
    },
    {
      payAmount: 734,
      getMoney: 1000,
      expect: {
        'bankNotes100': 2,
        'bankNotes50': 1,
        'coins10': 1,
        'coins5': 1,
        'coins1': 1,
      },
    },
  ]

  testCases.forEach((test) => {
    it(`payAmount is : ${test.payAmount} and receive money: ${test.getMoney} which is: ${
      test.expect
    }`, async () => {
      const result = await service.ExchangeLogic(test.payAmount, test.getMoney)
      expect(result).toEqual(test.expect)
    })
  })
})