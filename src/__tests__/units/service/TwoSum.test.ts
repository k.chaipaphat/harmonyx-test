/* eslint-disable no-magic-numbers */
import { TwoSumIntegerService } from '../../../services/TwoSumIntegerService'
const service = new TwoSumIntegerService()

describe('Test happy case TwoSum function', () => {
  const testCases = [
    {
      arrayNumber: [5, 1, 3, 2],
      target: 7,
      expect: [0, 3],
    },
    {
      arrayNumber: [5, 1, 3, 2],
      target: 6,
      expect: [0, 1],
    },
    {
      arrayNumber: [5, 1, 3, 2],
      target: 4,
      expect: [1, 2],
    },
    {
      arrayNumber: [2, 7, 11, 15],
      target: 9,
      expect: [0, 1],
    },
    {
      arrayNumber: [3, 2, 4],
      target: 6,
      expect: [1, 2],
    },
  ]

  testCases.forEach((test) => {
    it(`arrayNumber is : ${test.arrayNumber} and target is ${test.target} which is: ${
      test.expect
    }`, async () => {
      const result = await service.twoSumIntegers(test.arrayNumber, test.target)
      expect(result).toEqual(test.expect)
    })
  })
})

describe('Test worst case TwoSum function', () => {
  const testFailCases = [
    {
      arrayNumber: [5, 1, 3, 2],
      target: 200,
    },
    {
      arrayNumber: [5, 1, 3, 2],
      target: 300,
    },
    {
      arrayNumber: [5, 1, 3, 2],
      target: 400,
    },
    {
      arrayNumber: [2, 7, 11, 15],
      target: 900,
    },
    {
      arrayNumber: [3, 2, 4],
      target: 600,
    },
  ]

  testFailCases.forEach((test) => {
    it(`arrayNumber is : ${test.arrayNumber} and target is ${test.target} which is: Error`, async () => {
      try {
        await service.twoSumIntegers(test.arrayNumber, test.target)
      } catch (e) {
        expect(e).toEqual('Target and number of array is not match')
      }
    })
  })
})