/* eslint-disable no-magic-numbers */
import { ExchangeLogic } from '../../../logic/ExchangeService'
const service = new ExchangeLogic()

describe('Test happy case Exchange service', () => {
  const testCases = [
    {
      payAmount: 35,
      getMoney: 100,
      expect: [
        0, 0, 0, 1,
        0, 1, 1,
      ],
    },
    {
      payAmount: 319,
      getMoney: 500,
      expect: [
        0, 0, 1, 1, 1,
        1, 0, 0, 1,
      ],
    },
    {
      payAmount: 734,
      getMoney: 1000,
      expect: [
        0, 0, 2, 1, 0,
        1, 1, 0, 1,
      ],
    },
  ]

  testCases.forEach((test) => {
    it(`payAmount is : ${test.payAmount} and receive money: ${test.getMoney} which is: ${
      test.expect
    }`, async () => {
      const result = await service.ExchangeLogic(test.payAmount, test.getMoney)
      expect(result).toEqual(test.expect)
    })
  })
})

describe('Test worst case Exchange service', () => {
  const testFailCases = [
    {
      payAmount: 100,
      getMoney: 35,
      expect: 'Insufficient balance Your payment balance is 100',
    },
    {
      payAmount: 1000,
      getMoney: 999,
      expect: 'Insufficient balance Your payment balance is 1000',
    },
    {
      payAmount: 1000,
      getMoney: 1000,
      expect: {
        'message': 'Exact change',
      },
    },
  ]

  testFailCases.forEach((test) => {
    it(`payAmount is : ${test.payAmount} and receive money: ${test.getMoney} which is: ${
      test.expect
    }`, async () => {
      try {
        await service.ExchangeLogic(test.payAmount, test.getMoney)
      } catch (e) {
        expect(e).toEqual(test.expect)
      }
    })
  })
})