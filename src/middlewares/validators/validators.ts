/* eslint-disable @typescript-eslint/no-explicit-any */
import { check, validationResult } from 'express-validator'
import { UNPROCESSABLE_CODE } from '../../interface/HandleResponse'

export const validateExchange = [
  check('payAmount')
    .isNumeric()
    .withMessage('payAmount should be number type')
    .bail(),
  (req:any, res:any, next:any) => {
    const errors = validationResult(req)
    if (!errors.isEmpty())
    {return res.status(UNPROCESSABLE_CODE).json({errors: errors.array()})}
    next()
  },
  check('getMoney')
    .isNumeric()
    .withMessage('getMoney should be number type')
    .bail(),
  (req:any, res:any, next:any) => {
    const errors = validationResult(req)
    if (!errors.isEmpty())
    {return res.status(UNPROCESSABLE_CODE).json({errors: errors.array()})}
    next()
  },
]

export const validateTwoSum = [
  check('target')
    .isNumeric()
    .withMessage('target should be number type')
    .bail(),
  (req:any, res:any, next:any) => {
    const errors = validationResult(req)
    if (!errors.isEmpty())
    {return res.status(UNPROCESSABLE_CODE).json({errors: errors.array()})}
    next()
  },
]