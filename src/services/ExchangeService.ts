/* eslint-disable no-extra-parens */
/* eslint-disable no-magic-numbers */
'use strict'
import { ChangeInterface, KeepTheChangeInterface } from '../interface/ExchangeInterface'
import { ExchangeLogic } from '../logic/ExchangeService'
export class ExchangeService {
  async ExchangeLogic(payAmount: number, getMoney: number):
  Promise<ChangeInterface | KeepTheChangeInterface | ErrorCallback> {
    const KEEP_THE_CHANGE = 0
    if (getMoney < payAmount) {
      throw `Insufficient balance Your payment balance is ${payAmount}`
    }
    const change:number = getMoney - payAmount
    if (change === KEEP_THE_CHANGE) {
      const exactChange:KeepTheChangeInterface = {
        message: 'Exact change',
      }
      return exactChange
    }
    const logicExchange = new ExchangeLogic()
    const result = await logicExchange.ExchangeLogic(payAmount, getMoney)
    if (result) {
      const changeAmount:ChangeInterface = {
        bankNotes1000: result[0] === 0 ? undefined : result[0],
        bankNotes500: result[1] === 0 ? undefined : result[1],
        bankNotes100: result[2] === 0 ? undefined : result[2],
        bankNotes50: result[3] === 0 ? undefined : result[3],
        bankNotes20: result[4] === 0 ? undefined : result[4],
        coins10: result[5] === 0 ? undefined : result[5],
        coins5: result[6] === 0 ? undefined : result[6],
        coins2: result[7] === 0 ? undefined : result[7],
        coins1: result[8] === 0 ? undefined : result[8],
      }
      return changeAmount
    } else {
      throw 'An error occurred'
    }
  }
}