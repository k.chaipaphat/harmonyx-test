'use strict'

export class TwoSumIntegerService {
  async twoSumIntegers(arrayNumber: number[], target: number): Promise<[number, number] | ErrorCallback> {
    const DIFF_INDEX = -1
    const ARRAY_NUMBER_LENGTH = arrayNumber.length
    for (let index = 0; index < ARRAY_NUMBER_LENGTH; index++) {
      const diff = target - arrayNumber[index]
      const checkIndex = arrayNumber.indexOf(diff)

      if (checkIndex !== DIFF_INDEX && checkIndex !== index) {
        return [index, checkIndex]
      }
    }
    throw 'Target and number of array is not match'
  }
}