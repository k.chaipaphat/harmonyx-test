/* eslint-disable no-extra-parens */
/* eslint-disable no-magic-numbers */
'use strict'
export class ExchangeLogic {
  async ExchangeLogic(payAmount: number, getMoney: number):
  Promise<number[]> {
    const MONEY_CHANGE = [1, 2, 5, 10, 20, 50, 100, 500, 1000]
    const KEEP_THE_CHANGE = 0
    const arrayChange = []
    let MONEY_CHANGE_LENGTH = MONEY_CHANGE.length
    let change:number = getMoney - payAmount
    while (change > KEEP_THE_CHANGE) {
      MONEY_CHANGE_LENGTH--
      const changeAmount = Math.floor(change / MONEY_CHANGE[MONEY_CHANGE_LENGTH])
      change = change - (changeAmount * MONEY_CHANGE[MONEY_CHANGE_LENGTH])
      arrayChange.push(changeAmount)
    }
    return arrayChange
  }
}