/* eslint-disable no-magic-numbers */
'use strict'

import * as express from 'express'
import { routes } from './routes/api'

const app = express()

app.use(express.json())
app.use(routes)

app.get('/', (req, res) => {
  res.send('Chaipaphat Test')
})

app.listen(3000, () => console.log(''))
