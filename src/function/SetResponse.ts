/* eslint-disable @typescript-eslint/ban-types */
'use strict'
import { HandleResponse } from '../interface/HandleResponse'
export class SetResponse {
  static getErrorMessage(error: unknown): string {
    if (error instanceof Error) {return error.message}
    return String(error)
  }

  static setErrorResponse(code: number, message: string): HandleResponse {
    const result:HandleResponse = {
      code: code,
      message: message,
    }
    return result
  }
  static setSuccessResponse(code: number, message: string, data:{}): HandleResponse {
    const result:HandleResponse = {
      code: code,
      message: message,
      data: data,
    }
    return result
  }
}
