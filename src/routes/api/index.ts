import { Router } from 'express'
import { TwoSumIntegerController } from '../../controllers/TwoSumIntegerController'
import { ExchangeMachineController } from '../../controllers/ExchangeMachineController'
import { validateExchange, validateTwoSum} from '../../middlewares/validators/validators'
const routes = Router()

routes.post('/two_sum', validateTwoSum, new TwoSumIntegerController().twoSumIntegers)
routes.post('/exchange_vending_machine', validateExchange, new ExchangeMachineController().exchangeVendingMachine)

export { routes }