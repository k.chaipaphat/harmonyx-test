'use strict'
import { Request, Response } from 'express'
import { ExchangeService } from '../services/ExchangeService'
import { SetResponse } from '../function/SetResponse'
import { HandleResponse, SUCCESS_CODE, ERROR_CODE} from '../interface/HandleResponse'

export class ExchangeMachineController {
  async exchangeVendingMachine(req: Request, res: Response) {
    try {
      const { payAmount, getMoney } = req.body
      const service = new ExchangeService()
      const result = await service.ExchangeLogic(payAmount, getMoney)
      const resultSuccess:HandleResponse = SetResponse.setSuccessResponse(SUCCESS_CODE, 'Success', result)
      return res.status(SUCCESS_CODE).json(resultSuccess)
    }
    catch (error) {
      const message = SetResponse.getErrorMessage(error)
      const resultError:HandleResponse = SetResponse.setErrorResponse(ERROR_CODE, message)
      return res.status(ERROR_CODE).json(resultError)
    }
  }
}