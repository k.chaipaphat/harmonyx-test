'use strict'
import { Request, Response } from 'express'
import { TwoSumIntegerService } from '../services/TwoSumIntegerService'
import { SetResponse } from '../function/SetResponse'
import { HandleResponse, SUCCESS_CODE, ERROR_CODE} from '../interface/HandleResponse'

export class TwoSumIntegerController {
  async twoSumIntegers(req: Request, res: Response) {
    try {
      const { arrayNumber, target } = req.body
      const service = new TwoSumIntegerService()

      const result = await service.twoSumIntegers(arrayNumber, target)
      const resultSuccess:HandleResponse = SetResponse.setSuccessResponse(SUCCESS_CODE, 'Success', result)
      return res.status(SUCCESS_CODE).json(resultSuccess)
    }
    catch (error) {
      const message = SetResponse.getErrorMessage(error)
      const resultError:HandleResponse = SetResponse.setErrorResponse(ERROR_CODE, message)
      return res.status(ERROR_CODE).json(resultError)
    }
  }
}