/* eslint-disable @typescript-eslint/ban-types */
'use strict'
export const SUCCESS_CODE = 200
export const ERROR_CODE = 400
export const UNPROCESSABLE_CODE = 422

export interface HandleResponse {
    code: number;
    message: string;
    data?:{}
}

export default HandleResponse