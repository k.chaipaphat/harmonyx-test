'use strict'

export interface ChangeInterface {
    bankNotes1000?: number,
    bankNotes500?: number,
    bankNotes100?: number,
    bankNotes50?: number,
    bankNotes20?: number,
    coins10?: number,
    coins5?: number,
    coins2?: number,
    coins1?: number,
}

export interface KeepTheChangeInterface {
  message: string
}