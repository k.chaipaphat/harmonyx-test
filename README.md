## Installation

```bash
yarn
```

## Run with Docker

```bash
docker-compose up -d
```
## Run with dependency

```bash
yarn start.dev
```

## Run unit test

```bash
yarn test.unit
```

## Postman Collection

```bash
import Harmonyx.postman_collection.json to the Postman
```

## Swagger Api Spec

```bash
Can view in gitlab or swagger-api.yaml
```

url Swagger [url Swagger](https://gitlab.com/k.chaipaphat/harmonyx-test/-/blob/master/swagger-api.yaml)